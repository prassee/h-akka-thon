name := "akka-intro-talk"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-actor" % "2.4.14", "org.apache.parquet" % "parquet-avro" % "1.9.0")
