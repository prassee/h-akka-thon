import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.actor.Actor.Receive
import akka.routing._

case class DeviceData(deviceId: String, data: List[String])

class Device(backend: ActorRef) extends Actor {
  override def receive: PartialFunction[Any, Unit] = {
    case rawData: (String, String) => backend ! DeviceData(rawData._1, rawData._2.split(",").toList)
  }
}

class AdxLoadBalancingActor extends Actor {

  private val router: Router = {
    val routees = 5
    val actorRefRoutees = Vector.fill(routees) {
      val r = context.actorOf(Props(classOf[AdxBackend]))
      context watch r
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), actorRefRoutees)
  }

  override def receive: Receive = {
    case data : (String,List[String]) => router.route(???, sender())
  }
}

class AdxBackend extends Actor {
  override def receive: Receive = {
    case data: DeviceData => println(data)
  }
}

object AkkaDemo extends App {
  val as = ActorSystem("hakkathon")
  // initialize  AdxBackend back
  val adxBackend = as.actorOf(Props[AdxBackend], "adxBackEnd")

  // init device actor , send   backend actor as cons param to device actor
  // send data to device actor

}
